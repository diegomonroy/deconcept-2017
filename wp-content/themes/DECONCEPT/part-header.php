<!-- Begin Top -->
	<section class="top wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<div class="row collapse align-justify align-middle line">
					<div class="small-12 medium-5 columns">
						<?php dynamic_sidebar( 'login_top' ); ?>
					</div>
					<div class="small-12 medium-5 columns">
						<?php dynamic_sidebar( 'cart_top' ); ?>
					</div>
				</div>
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->