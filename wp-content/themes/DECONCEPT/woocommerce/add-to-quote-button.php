<?php

/**
 * Add to Quote button template
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @author  Yithemes
 */

?>
<div class="quote">
	<div class="row align-center align-middle">
		<div class="small-4 columns">
			Escríbenos un<br />
			asesor te contactará
		</div>
		<div class="small-8 columns text-center">
			<a href="#" class="<?php echo $class; ?>" data-product_id="<?php echo $product_id; ?>" data-wp_nonce="<?php echo $wpnonce; ?>">
				<?php echo $label; ?>
			</a>
			<img src="<?php echo esc_url( YITH_YWRAQ_ASSETS_URL . '/images/wpspin_light.gif' ); ?>" class="ajax-loading" alt="Cargando" width="16" height="16" style="visibility: hidden;" />
		</div>
	</div>
</div>