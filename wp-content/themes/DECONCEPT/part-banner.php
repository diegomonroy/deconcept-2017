<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'tendencias' ) ) ) : dynamic_sidebar( 'banner_tendencias' ); endif; ?>
				<?php if ( is_page( array( 'pisos' ) ) ) : dynamic_sidebar( 'banner_pisos' ); endif; ?>
				<?php if ( is_page( array( 'pisos-laminados' ) ) ) : dynamic_sidebar( 'banner_laminados' ); endif; ?>
				<?php if ( is_page( array( 'pisos-pvc' ) ) ) : dynamic_sidebar( 'banner_pvc' ); endif; ?>
				<?php if ( is_page( array( 'alfombras' ) ) ) : dynamic_sidebar( 'banner_alfombras' ); endif; ?>
				<?php if ( is_page( array( 'alfombras-comercial' ) ) ) : dynamic_sidebar( 'banner_comercial' ); endif; ?>
				<?php if ( is_page( array( 'alfombras-hogar' ) ) ) : dynamic_sidebar( 'banner_hogar' ); endif; ?>
				<?php /*if ( is_page( array( 'tapetes-decorativos' ) ) ) : dynamic_sidebar( 'banner_tapetes_decorativos' ); endif;*/ ?>
				<?php if ( is_page( array( 'otros' ) ) ) : dynamic_sidebar( 'banner_otros' ); endif; ?>
				<?php if ( is_page( array( 'desarrollo-de-ideas' ) ) ) : dynamic_sidebar( 'banner_desarrollo_de_ideas' ); endif; ?>
				<?php if ( is_page( array( 'encuentranos' ) ) ) : dynamic_sidebar( 'banner_encuentranos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->