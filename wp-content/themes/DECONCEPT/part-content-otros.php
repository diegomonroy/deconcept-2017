<?php get_template_part( 'part', 'banner' ); ?>
<!-- Begin Content -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 medium-3 columns">
				<?php get_template_part( 'part', 'search-widget' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->