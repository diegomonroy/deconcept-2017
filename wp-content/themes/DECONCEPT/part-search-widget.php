<!-- Begin Search Widget -->
	<div class="search_widget">
		<style>
			<?php if ( is_page( array( 'pisos-laminados' ) ) ) : ?>
			/*option[value=four] {display: none;}*/
			.woof_container.woof_container_ensamble,
			.woof_container.woof_container_tamao,
			.woof_container.woof_container_fibra,
			.woof_container.woof_container_textura { display: none !important; }
			.woof_redraw_zone .woof_container.woof_container_acabados { order: 1; }
			.woof_redraw_zone .woof_container.woof_container_superficie { order: 2; }
			.woof_redraw_zone .woof_container.woof_container_trficos { order: 3; }
			.woof_redraw_zone .woof_container.woof_container_espesor { order: 4; }
			.woof_redraw_zone .woof_submit_search_form_container { order: 5; }
			<?php endif; ?>
			<?php if ( is_page( array( 'pisos-pvc' ) ) ) : ?>
			.woof_container.woof_container_superficie,
			.woof_container.woof_container_tamao,
			.woof_container.woof_container_fibra,
			.woof_container.woof_container_textura { display: none !important; }
			.woof_redraw_zone .woof_container.woof_container_ensamble { order: 1; }
			.woof_redraw_zone .woof_container.woof_container_acabados { order: 2; }
			.woof_redraw_zone .woof_container.woof_container_trficos { order: 3; }
			.woof_redraw_zone .woof_container.woof_container_espesor { order: 4; }
			.woof_redraw_zone .woof_submit_search_form_container { order: 5; }
			<?php endif; ?>
			<?php if ( is_page( array( 'alfombras-comercial' ) ) ) : ?>
			.woof_container.woof_container_espesor,
			.woof_container.woof_container_ensamble,
			.woof_container.woof_container_superficie,
			.woof_container.woof_container_tamao,
			.woof_container.woof_container_acabados,
			.woof_container.woof_container_trficos { display: none !important; }
			.woof_redraw_zone .woof_container.woof_container_textura { order: 1; }
			.woof_redraw_zone .woof_container.woof_container_fibra { order: 2; }
			.woof_redraw_zone .woof_submit_search_form_container { order: 3; }
			<?php endif; ?>
			<?php if ( is_page( array( 'alfombras-hogar' ) ) ) : ?>
			.woof_container.woof_container_espesor,
			.woof_container.woof_container_ensamble,
			.woof_container.woof_container_superficie,
			.woof_container.woof_container_tamao,
			.woof_container.woof_container_acabados,
			.woof_container.woof_container_trficos { display: none !important; }
			.woof_redraw_zone .woof_container.woof_container_textura { order: 1; }
			.woof_redraw_zone .woof_container.woof_container_fibra { order: 2; }
			.woof_redraw_zone .woof_submit_search_form_container { order: 3; }
			<?php endif; ?>
			<?php if ( is_page( array( 'tapetes-decorativos' ) ) ) : ?>
			.woof_container.woof_container_espesor,
			.woof_container.woof_container_ensamble,
			.woof_container.woof_container_superficie,
			.woof_container.woof_container_acabados,
			.woof_container.woof_container_fibra,
			.woof_container.woof_container_textura,
			.woof_container.woof_container_trficos { display: none !important; }
			<?php endif; ?>
		</style>
		<?php dynamic_sidebar( 'search_widget' ); ?>
	</div>
<!-- End Search Widget -->