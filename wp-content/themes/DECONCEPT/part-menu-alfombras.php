<!-- Begin Menu Alfombras -->
	<section class="menu_products wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<div class="moduletable_ma1">
					<?php
					wp_nav_menu(
						array(
							'menu_class' => 'menu align-center',
							'container' => false,
							'theme_location' => 'alfombras-menu',
							'items_wrap' => '<ul class="%2$s">%3$s</ul>'
						)
					);
					?>
				</div>
			</div>
		</div>
	</section>
<!-- End Menu Alfombras -->