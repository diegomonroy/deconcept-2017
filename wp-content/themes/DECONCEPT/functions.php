<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'alfombras-menu' => __( 'Alfombras Menu' ),
			'main-menu' => __( 'Main Menu' ),
			'pisos-menu' => __( 'Pisos Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Login Top',
			'id' => 'login_top',
			'before_widget' => '<div class="moduletable_to2 text-left">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Cart Top',
			'id' => 'cart_top',
			'before_widget' => '<div class="moduletable_to3 text-right">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Tendencias',
			'id' => 'banner_tendencias',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Pisos',
			'id' => 'banner_pisos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Laminados',
			'id' => 'banner_laminados',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner PVC',
			'id' => 'banner_pvc',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Alfombras',
			'id' => 'banner_alfombras',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Comercial',
			'id' => 'banner_comercial',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Hogar',
			'id' => 'banner_hogar',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Tapetes Decorativos',
			'id' => 'banner_tapetes_decorativos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Otros',
			'id' => 'banner_otros',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Desarrollo de Ideas',
			'id' => 'banner_desarrollo_de_ideas',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Encuéntranos',
			'id' => 'banner_encuentranos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Widget',
			'id' => 'search_widget',
			'before_widget' => '<div class="moduletable_sw1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Newsletter',
			'id' => 'newsletter',
			'before_widget' => '<div class="row align-center align-middle moduletable_n1">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 12;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Custom hooks in WooCommerce
 */

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

function custom_description() {
	global $product;
	the_content();
	echo '<div class="price">' . $product->get_price_html() . '</div>';
	if ( get_field( 'ficha_tecnica' ) ) {
		echo '
			<div class="data_sheet">
				<div class="row align-center align-middle">
					<div class="small-4 columns">
						Descarga<br />
						Ficha Técnica
					</div>
					<div class="small-8 columns text-center">
						<a href="' . get_field( 'ficha_tecnica' ) . '" target="_blank"><img src="' . get_site_url() . '/wp-content/themes/DECONCEPT/build/icon_download.png" title="Ficha Técnica" alt="Ficha Técnica"></a>
					</div>
				</div>
			</div>
		';
	}
	$category_id = wc_get_product_category_list( $product->get_id(), $product->get_category_ids() );
	switch ( $category_id ) {
		default:
			break;
		case 144:
		case 157:
		case 273:
		case 160:
		case 158:
		case 159:
		case 155:
		case 145:
		case 156:
		case 154:
		case 150:
		case 287:
		case 167:
			echo '<div class="buy">';
			do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' );
			echo '</div>';
			echo '<style> .quote { display: none !important; } </style>';
			break;
	}
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	echo '<div class="clear"></div>';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );

function custom_attributes() {
	global $product;
	echo '<div class="row align-center">';
	$category_id = wc_get_product_category_list( $product->get_id(), $product->get_category_ids() );
	switch ( $category_id ) {
		default:
			$column_style = '';
			break;
		case 17:
		case 18:
		case 21:
		case 22:
		case 85:
		case 19:
		case 92:
		case 93:
		case 20:
		case 153:
			$column_style = 'medium-6';
			echo '<div class="small-12 ' . $column_style . ' columns attributes_colors">';
			do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' );
			echo '</div>';
			break;
	}
	echo '<div class="small-12 ' . $column_style . ' columns">';
	echo '<h2 class="attributes">DESCRIPCIÓN</h2>';
	$product->list_attributes();
	echo '</div>';
	echo '</div>';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_attributes', 20 );

function custom_bottom() {
	echo '
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_bottom', 30 );

/*
 * Custom shortcode to Log In
 */
function shortcode_log_in() {
	$web = get_site_url() . '/';
	if ( is_user_logged_in() ) {
		global $current_user;
		get_currentuserinfo();
		$username = $current_user->user_login;
		$my_account = $web . 'mi-cuenta/';
		$my_orders = $web . 'tienda/mi-cuenta/';
		$logout = $web . 'usuario/cerrar-sesion/';
		$html = 'Bienvenido/a! ' . $username . ' | <a href="' . $my_account . '">Mi Cuenta</a> | <a href="' . $my_orders . '">Mis Pedidos</a> | <a href="' . $logout . '">Cerrar Sesión</a>';
	} else {
		$login = $web . 'usuario/inicia-sesion/';
		$register = $web . 'usuario/registrate-aqui/';
		$html = 'Bienvenido/a! <a href="' . $login . '">Inicia Sesión</a> o <a href="' . $register . '">Regístrate Aquí</a>';
	}
	return $html;
}
add_shortcode( 'log_in', 'shortcode_log_in' );