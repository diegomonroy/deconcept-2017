<?php get_template_part( 'part', 'banner' ); ?>
<!-- Begin Content -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'woocommerce-main' ); ?>
			</div>
		</div>
	</section>
<!-- End Content -->